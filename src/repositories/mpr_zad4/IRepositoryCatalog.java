package mpr_zad4;

import mpr_zad4.mpr_zad4.Order;

public interface IRepositoryCatalog {

	public IOrderRepository getUsers();
	public IRepository<Order> getPersons();
	public void commit();
}