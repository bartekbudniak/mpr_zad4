package mpr_zad4.mpr_zad4;


import java.util.List;

public class Order {

	private long id;
	private ClientDetails client;
	private Address deliveryAddress;
	private List<OrderItem> items;
	
	
	public Order(){
		
	}
	
	public Order(ClientDetails client, Address deliveryAddress) {
		super();
		this.client = client;
		this.deliveryAddress = deliveryAddress;
	}
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public ClientDetails getClient() {
		return client;
	}
	public void setClient(ClientDetails client) {
		this.client = client;
	}
	public Address getDeliveryAddress() {
		return deliveryAddress;
	}
	public void setDeliveryAddress(Address deliveryAddress) {
		this.deliveryAddress = deliveryAddress;
	}
	public List<OrderItem> getItems() {
		return items;
	}
	public void setItems(List<OrderItem> items) {
		this.items = items;
	}
}
